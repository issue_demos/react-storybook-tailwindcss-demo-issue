// const defaultTheme = require("tailwindcss/defaultTheme");
const config = require('./config/tailwind');

module.exports = {
  ...config,
  content: ['./src/**/*.{js,ts,jsx,tsx}'],
};
