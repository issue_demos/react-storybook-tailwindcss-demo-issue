import { memo } from 'react';
import cx from 'clsx';

import styles from './TestButton.module.scss';

export interface TestButtonProps {}

const TestButton = (props: TestButtonProps) => {
  const { className, foo } = props;
  return <div className={cx(styles.TestButton, className, 'bg-blue-500 hover:bg-blue-700 h-16 w-16')}>{foo}</div>;
};

export default memo(TestButton);
