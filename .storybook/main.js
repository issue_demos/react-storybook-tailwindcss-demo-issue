const path = require('path');
// const { tsconfigResolveAliases } = require('../config/webpack-helpers');

// TODO: extract to `config` workspace
const sassOptions = {
  includePaths: [path.resolve(__dirname, 'node_modules'), path.resolve(__dirname, 'src/*/node_modules')],
};

module.exports = {
  stories: ['../src/**/*.stories.mdx', '../src/**/*.stories.@(js|jsx|ts|tsx)'],
  addons: [
    // Add any Storybook addons you want here: https://storybook.js.org/addons/
    '@storybook/addon-links',
    '@storybook/addon-essentials',
    {
      name: '@storybook/addon-postcss',
      options: {
        postcssLoaderOptions: {
          implementation: require('postcss'),
        },
      },
    },
  ],
  core: {
    builder: 'webpack5',
  },
  framework: '@storybook/react',
  webpackFinal: async (config) => {
    // resolve problem with typescript aliases
    config.resolve.alias = {
      ...config.resolve?.alias,
      // ...tsconfigResolveAliases(path.resolve(__dirname, '../tsconfig.json')),
    };

    // Custom aliases (TODO:)
    // config.resolve.alias.effector = path.resolve('./node_modules/effector/effector.cjs.js');
    // config.resolve.alias['effector-react/ssr'] = path.resolve('./node_modules/effector-react/effector-react.cjs');
    // config.resolve.alias['effector-react'] = path.resolve('./node_modules/effector-react/effector-react.cjs');

    config.resolve.extensions.push('.ts', '.tsx');

    // SCSS (global) TODO: configure resolve-url-loader, sass-loader with global sassOptions
    config.module.rules.push({
      test: /\.scss$/,
      include: path.resolve(__dirname, '../src'),
      use: [
        'style-loader',
        'css-loader',
        {
          loader: 'resolve-url-loader',
          options: {
            sourceMap: true,
          },
        },
        {
          loader: 'sass-loader',
          options: {
            sourceMap: true,
            sassOptions,
          },
        },
      ],
    });

    // SCSS modules
    config.module.rules.push({
      test: /\.module\.scss$/,
      include: path.resolve(__dirname, '../src/'),
      use: [
        // 'style-loader',
        {
          loader: 'css-loader',
          options: {
            importLoaders: 1,
            modules: {
              // exportLocalsConvention: 'camelCase',
              localIdentName: '[name]__[local]___[hash:base64:5]',
            },
          },
        },
        {
          loader: 'postcss-loader',
        },
        {
          loader: 'resolve-url-loader',
          options: {
            sourceMap: true,
          },
        },
        {
          loader: 'sass-loader',
          options: {
            sourceMap: true,
            sassOptions,
          },
        },
        // {
        //   loader: 'sass-resources-loader',
        //   options: {
        //     // globally needed mixins, functions, etc.
        //     // eslint-disable-next-line global-require
        //     resources: require('../src/scss/scss-resources.js'),
        //   },
        // },
      ],
    });

    // TODO: SVG config

    return config;
  },
};
