// const defaultTheme = require("tailwindcss/defaultTheme");
module.exports = {
  theme: {
    extend: {
      container: {
        center: true,
        padding: {
          DEFAULT: '0.9375rem', // 15px
          md: '1rem', // 16px
          xl: '3.4375rem', // 55px
        },
      },
      screens: {
        sm: '640px',
        md: '768px',
        lg: '1024px',
        xl: '1280px',
      },
      fontFamily: {
        sans: ['Acrom', 'Helvetica', 'sans-serif'],
        serif: ['georgia', 'Times New Roman', 'times', 'serif'],
      },
      colors: {
        primary: '#ffda7c',
        secondary: '#fe7375',
        success: '#58dd4d',
        danger: '#fe7375',
      },
    },
  },
  plugins: [],
};
