const fs = require('fs');
const path = require('path');
const JSONC = require('jsonc-parser');
const assert = require('assert');
/**
 * Source: https://github.com/microsoft/pyright/blob/169c6089b9914870c43622743c75844657cd9d70/build/lib/webpack.js#L90
 * Builds a webpack resolve alias configuration from a tsconfig.json file.
 * This is an alternative to using tsconfig-paths-webpack-plugin, which
 * has a number of unfixed bugs.
 *
 * https://github.com/dividab/tsconfig-paths/issues/143
 * https://github.com/dividab/tsconfig-paths-webpack-plugin/issues/59
 * https://github.com/dividab/tsconfig-paths-webpack-plugin/issues/60
 * @param {string} tsconfigPath Path to tsconfig
 */
function tsconfigResolveAliases(tsconfigPath) {
  tsconfigPath = path.resolve(tsconfigPath);
  const tsconfigDir = path.dirname(tsconfigPath);

  const tsconfig = JSONC.parse(fs.readFileSync(tsconfigPath, 'utf-8'));
  const baseUrl = tsconfig.baseUrl;
  assert(typeof baseUrl === 'string' || baseUrl === undefined);
  const baseDir = baseUrl ? path.resolve(tsconfigDir, baseUrl) : tsconfigDir;

  // XXX Not worked if no compilerOptions defined
  const paths = tsconfig['compilerOptions']['paths'];
  assert(typeof paths === 'object');

  const endWildcard = /\/\*$/;

  return Object.fromEntries(
    Object.entries(paths).map(([from, toArr]) => {
      assert(typeof from === 'string', typeof from);
      assert(Array.isArray(toArr) && toArr.length === 1);
      let to = toArr[0];
      assert(typeof to === 'string');
      assert(endWildcard.test(from));
      assert(endWildcard.test(to));

      from = from.replace(endWildcard, '');
      to = to.replace(endWildcard, '');

      return [from, path.resolve(baseDir, to)];
    }),
  );
}

module.exports = {
  tsconfigResolveAliases,
};
